from website import create_app
import os

app = create_app()

if __name__ == '__main__':
    # Create an 'uploads' directory if it doesn't exist
    if not os.path.exists('/var/tmp/uploads'):
        os.makedirs('/var/tmp/uploads')

    # Create a reports directory if it doesn't exist
    if not os.path.exists('/var/tmp/reports'):
        os.makedirs('/var/tmp/reports')
    app.run(debug=True)