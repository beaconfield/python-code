from flask import Blueprint, render_template, request, send_file
import hashlib
import os
import uuid
import re
from datetime import datetime

# import reportlab in order to generate pdf documents
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter, landscape
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle

views = Blueprint('views', __name__)

# python function to compute the hash of a given file
#def compute_sha256(file_path):
#    sha256_hash = hashlib.sha256()
#    with open(file_path, "rb") as f:
#        while chunk := f.read(4096):
#            sha256_hash.update(chunk)
#    return sha256_hash.hexdigest()

# Function to compute the hash of a file using the selected algorithm
def compute_hash(file_path, algorithm):
    hash_func = hashlib.new(algorithm)
    with open(file_path, "rb") as f:
        while chunk := f.read(4096):
            hash_func.update(chunk)
    return hash_func.hexdigest()

# Function to validate the user-submitted hash
def validate_hash(hash_str):
    # Regular expression pattern for hexidecimal characters
    pattern = re.compile(r'^[a-fA-F0-9]{1,128}$')
    return bool(pattern.match(hash_str))

# Function to generate a PDF report
def generate_pdf_report(file_name, user_hash, result, computed_hash, algorithm):
    pdf_file_path = "/var/tmp/reports/file_integrity_report.pdf"  # Path to save the PDF report
    doc = SimpleDocTemplate(pdf_file_path, pagesize=landscape(letter))
    styles = getSampleStyleSheet()
    style_heading = styles['Heading1']
    style_heading.alignment = 1
    style_body = styles['BodyText']

    # Define a style for content that should be monospaced
    style_monospaced = styles['BodyText']
    style_monospaced.fontName = 'Courier'

    report_content = []
    # Add the report title
    report_content.append(Paragraph("File Integrity Verification Report", style_heading))
    report_content.append(Spacer(1, 12))

    # Add the file name
    file_name_paragraph = Paragraph(f"<b>Uploaded Filename:</b> {file_name}", style_body)
    report_content.append(file_name_paragraph)

    # Add the algorithm used
    algorithm_paragraph = Paragraph(f"<b>Hash Algorithm:</b> {algorithm}", style_body)
    report_content.append(algorithm_paragraph)

    # Add the user submitted hash
    user_hash_paragraph = Paragraph(f"<b>User Submitted Hash:</b>", style_body)
    user_hash_value = Paragraph(f"{user_hash}", style_monospaced)
    report_content.extend([user_hash_paragraph, Spacer(1, 0), user_hash_value, Spacer(1, 12)])

    # Add the computed hash
    computed_hash_paragraph = Paragraph(f"<b>Computed Hash:</b>", style_body)
    computed_hash_value = Paragraph(f"{computed_hash}", style_monospaced)
    report_content.extend([computed_hash_paragraph, Spacer(1, 0), computed_hash_value, Spacer(1, 12)])

    # Add the validation result
    result_paragraph = Paragraph(f"<b>Validation Result:</b> <font color='#008000'>{result}</font>", style_body)
    report_content.append(result_paragraph)

    # Add the date and time
    date_time_paragraph = Paragraph(f"<b>Date and Time:</b> {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}", style_body)
    report_content.append(date_time_paragraph)

    doc.build(report_content)
    return pdf_file_path

@views.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        # Get the user-selected algorithm, user-submitted hash and the uploaded file
        #user_hash = request.form['hash']
        algorithm = request.form['algorithm']
        user_hash = request.form['hash'].lower()  # Convert to lowercase
        uploaded_file = request.files['file']
        file_name = uploaded_file.filename  # get the filename

        # Validate the user-submitted hash
        if not validate_hash(user_hash):
            return render_template('error.html', message='Invalid hash format. Check your hash value and try again.')

        # Generate a unique filename for the uploaded file
        file_uuid = str(uuid.uuid4())
        file_extension = os.path.splitext(uploaded_file.filename)[1]
        uploaded_file_path = os.path.join("/var/tmp/uploads", file_uuid + file_extension)

        # Save the uploaded file to disk
        uploaded_file.save(uploaded_file_path)

        # Compute the SHA256 hash of the uploaded file
        #computed_hash = compute_sha256(uploaded_file_path)

        # Compute the hash of the uploaded file using the selected algorithm
        computed_hash = compute_hash(uploaded_file_path, algorithm)

        # Compare the user-submitted hash with the computed hash
        if user_hash == computed_hash:
            result = "Hashes match! File integrity verified."

            # Generate PDF report
            pdf_file_path = generate_pdf_report(file_name, user_hash, result, computed_hash, algorithm)

            # Serve the PDF report for download by the user
            return send_file(pdf_file_path, as_attachment=True)
        else:
            result = "Hashes do not match! File integrity compromised."
            #print(user_hash + "is the user submitted hash.")
            #print(computed_hash + "is the computed hash.")
    
        # Render the result template with the verification result
        return render_template('result.html', result=result, computed_hash=computed_hash, user_hash=user_hash)
    return render_template("home.html")