import hashlib

def compute_sha256(file_path):
    """Compute the SHA256 hash of a file."""
    sha256_hash = hashlib.sha256()
    with open(file_path, "rb") as f:
        while chunk := f.read(4096):  # Read the file in chunks to avoid loading it entirely into memory
            sha256_hash.update(chunk)
    return sha256_hash.hexdigest()

def main():
    file_path = input("Enter the path of the file to hash: ")
    try:
        user_hash = input("Enter the SHA256 hash of the file: ").strip()
        computed_hash = compute_sha256(file_path)
        if user_hash == computed_hash:
            print("Hashes match! File integrity verified.")
        else:
            print("Hashes do not match! File integrity compromised.")
            print(f"Expected hash: {computed_hash}")
            print(f"Provided hash: {user_hash}")
    except FileNotFoundError:
        print("File not found.")

if __name__ == "__main__":
    main()
